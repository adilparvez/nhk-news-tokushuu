import { program } from "commander";
import { JSDOM } from "jsdom";
import fetch from "node-fetch";
import path from "path";

import {
    download,
    formatTimestamp,
    fromJson,
    mkdir,
    readUtf8,
    readUtf8Sync,
    sha256sum,
    sleep,
    startBrowser,
    toJson,
    writeUtf8,
} from "./utils";


const READABILITY_JS = readUtf8Sync("node_modules/@mozilla/readability/Readability.js");
const NHK_NEWS_URL = "https://www3.nhk.or.jp/news";

async function main(argv) {
    await mkdir("output");

    program.command("get-links")
        .action(getLinks);

    program.command("get-pages")
        .action(getPages);

    program.command("map-to-articles")
        .action(mapToArticles);

    program.command("download-images")
        .action(downloadImages);

    program.command("build-book")
        .action(buildBook);

    program.parse(argv);
}

async function getLinks() {
    const items = [];
    for (let i = 1;; i++) {
        const page = i.toString().padStart(3, "0");
        console.log(`Getting json ${page}`);
        const url = `${NHK_NEWS_URL}/json16/tokushu/new_tokushu_${page}.json`;

        const response = await fetch(url);
        const data = await response.json();

        items.push(...data.channel.item);

        if (!data.channel.hasNext) {
            break;
        }

        console.log("Sleeping 1s");
        await sleep(1000);
    }

    // {
    //     "id": "263248",
    //     "title": "「我慢、我慢、そして我慢」ニューヨーク ロックダウンの代償",
    //     "description": "この動画は、ニューヨークの「かつての姿」と、今を比べられるよう作ってみました。途中にある写真は、ことし3月以降の“ロックダウン”下のニューヨークで筆者が撮ったものです（メディアは通勤などの外出は認められていました）。\r\n新型コロナウイルスの感染拡大が、世界で最も深刻なアメリカ。中でも、当時いちばんひどかったのがニューヨークです。3月22日から、住民に原則自宅での待機を求めるなど、3か月近くにわたってほぼすべての経済活動を強制的に停止し、その後の感染爆発を何とか食い止めました。しかし、ロックダウンから半年がたち、周りを見れば、その代償がとても大きいことに気付きます。（アメリカ総局記者 野口修司）",
    //     "pubDate": "Fri, 25 Sep 2020 15:18:33 +0900",
    //     "cate": "5",
    //     "cate_group": [
    //         "5",
    //         "6"
    //     ],
    //     "link": "html/20200925/k10012632481000.html",
    //     "imgPath": "html/20200925/K10012632481_2009251207_2009251311_01_03.jpg",
    //     "iconPath": "html/20200925/K10012632481_2009251207_2009251311_02_01.jpg",
    //     "videoPath": "k10012632481_202009251122_202009251214.mp4",
    //     "videoDuration": "46",
    //     "relationNews": []
    // },

    const links = items.map((item, index) => ({
        index,
        url: `${NHK_NEWS_URL}/${item.link}`,
        item,
    }));

    console.log("Writing to output/links.json");
    await writeUtf8("output/links.json", toJson(links));
}

async function getPages() {
    const links = fromJson(await readUtf8("output/links.json"));
    const browser = await startBrowser();

    const pages = [];
    for (const link of links) {
        console.log(`Getting page ${link.index}`);
        const url = link.url;

        const page = await browser.newPage();
        await page.goto(url, {
            waitUntil: "networkidle0",
        });

        const readability = await page.evaluate(`
            (() => {
                ${READABILITY_JS}
                return new Readability(document).parse();
            })();
        `);

        pages.push({
            ...link,
            readability,
        });

        console.log("Sleeping 1s");
        await sleep(1000);
    }

    console.log("Writing to output/pages.json");
    await writeUtf8("output/pages.json", toJson(pages));

    await browser.close();
}

async function mapToArticles() {
    const pages = fromJson(await readUtf8("output/pages.json"));

    const articles = [];
    const imagesToFetch = [];

    for (const page of pages) {
        console.log(`Mapping page ${page.index}`);

        // Article thumbnail image.
        const thumbnailSrc = `${NHK_NEWS_URL}/${page.item.imgPath}`;
        const thumbnailFileName = `${sha256sum(thumbnailSrc)}.jpeg`;
        imagesToFetch.push({ url: thumbnailSrc, path: `{{IMAGE_DIR}}/${thumbnailFileName}` });
        const newThumbnailSrc = `file://{{IMAGE_DIR}}/${thumbnailFileName}`;

        // Images in article body.
        const document = new JSDOM(page.readability.content).window.document;
        document.querySelectorAll("img").forEach((element) => {
            const src = element.getAttribute("src");
            const fileName = `${sha256sum(src)}.jpeg`;
            imagesToFetch.push({ url: src, path: `{{IMAGE_DIR}}/${fileName}` });
            const newSrc = `file://{{IMAGE_DIR}}/${fileName}`;

            element.setAttribute("src", newSrc);
        });

        articles.push({
            index: page.index,
            url: page.url,
            title: page.item.title,
            time: formatTimestamp(page.item.pubDate),
            image: newThumbnailSrc,
            content: document.body.innerHTML,
        });
    }

    console.log("Writing to output/articles.json");
    await writeUtf8("output/articles.json", toJson(articles));

    console.log("Writing to output/images-to-fetch.json");
    await writeUtf8("output/images-to-fetch.json", toJson(imagesToFetch));
}

async function downloadImages() {
    await mkdir("output/images");
    const imageDir = path.resolve("output/images");

    const images = fromJson(await readUtf8("output/images-to-fetch.json"));
    const failures = [];

    for (const image of images) {
        try {
            const imagePath = image.path.replace("{{IMAGE_DIR}}", imageDir);
            console.log(`Downloading ${image.url} to ${imagePath}`);
            await download(image.url, imagePath);
        } catch {
            console.log("Failed");
            failures.push(image);
        }
        console.log("Sleeping 0.1s");
        await sleep(100);
    }

    if (failures.length > 0) {
        console.log("Writing to output/failures.json");
        await writeUtf8("output/failures.json", toJson(failures));
    }

    console.log("Done");
}

async function buildBook() {
    const imageDir = path.resolve("output/images");

    const articles = fromJson(await readUtf8("output/articles.json"));

    const body = articles.map((article) => {
        const image = article.image.replace("{{IMAGE_DIR}}", imageDir);
        const body = article.content.replace(/{{IMAGE_DIR}}/g, imageDir);

        return `
            <h1>${article.title}</h1>
            <img src="${image}">
            <p>${article.time}</p>
            ${body}
        `;
    }).join("\n");

    console.log("Writing to output/nhk.html");
    await writeUtf8("output/nhk.html", body);
}

main(process.argv);
